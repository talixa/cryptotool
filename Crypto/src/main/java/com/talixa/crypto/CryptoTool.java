package com.talixa.crypto;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileFilter;

import com.talixa.crypto.listeners.ClearTextAreaListener;
import com.talixa.crypto.listeners.CopyFromClipboardListener;
import com.talixa.crypto.listeners.CopyToClipboardListener;
import com.talixa.crypto.shared.CryptoConstants;
import com.talixa.cryptolib.Base64Encoder;
import com.talixa.cryptolib.CryptoToolkit;
import com.talixa.cryptolib.CryptoToolkit.Cipher;
import com.talixa.cryptolib.cipher.CryptoCipher;
import com.talixa.cryptolib.cipher.parameters.CryptoParameters;
import com.talixa.cryptolib.cipher.parameters.SymmetricCryptoParameters;
import com.talixa.swing.SwingApp;
import com.talixa.swing.frames.FrameAbout;
import com.talixa.swing.listeners.ExitActionListener;
import com.talixa.swing.shared.AboutInfo;
import com.talixa.swing.shared.AppInfo;

public class CryptoTool extends SwingApp {
	
	// widgets references
	private static JTextArea txtCrypto;
	private static JTextArea txtPlain;
	private static JPasswordField txtPassword;
	private static JRadioButton radDes;
	private static JRadioButton rad3Des;
	private static JRadioButton radAes;
	
	public static void main(String[] args) {
		// allow user to open only txt files
		fileChooser.setFileFilter(new FileFilter() {
			@Override
		    public boolean accept(File f){
		        return f.getName().toLowerCase().endsWith(".txt")||f.isDirectory();
		    }
			
			@Override
		    public String getDescription(){
		        return "Text Files (*.txt)";
		    }
		});
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// create app info bundle
				AppInfo app = getAppInfo();
				
				// create frame
				SwingApp.init(app);
				
				// add gui elements
				createGui();
				
				// start app
				SwingApp.createAndShowGUI(createMenuBar(), null);
			}
		});	
	}	
	
	private static void createGui() {
		// setup options panel on top
		JPanel pnlOpts = new JPanel();
		pnlOpts.add(new JLabel("Password:"));
		txtPassword = new JPasswordField(10);
		pnlOpts.add(txtPassword);
		
		// add buttons for encryption types
		radDes = new JRadioButton("DES");
		rad3Des = new JRadioButton("3DES");
		radAes = new JRadioButton("AES");
		ButtonGroup bgCrypto = new ButtonGroup();
		bgCrypto.add(radDes);
		bgCrypto.add(rad3Des);
		bgCrypto.add(radAes);
		bgCrypto.setSelected(radDes.getModel(), true);
		pnlOpts.add(radDes);
		pnlOpts.add(rad3Des);
		pnlOpts.add(radAes);
		
		// setup text panel in middle
		JPanel pnlText = new JPanel(new BorderLayout());
		pnlText.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		txtCrypto = createInputArea();
		txtPlain  = createInputArea();
		pnlText.add(new JScrollPane(txtPlain), BorderLayout.CENTER);
		pnlText.add(new JScrollPane(txtCrypto), BorderLayout.EAST);
		
		// setup buttons panel at bottom
		JPanel pnlButton = new JPanel(new BorderLayout());
		JButton btnEncrypt = new JButton("Encrypt");
		btnEncrypt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				encrypt();
			}
		});
		
		final JButton btnDecrypt = new JButton("Decrypt");
		btnDecrypt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				decrypt();
			}
		});
		pnlButton.add(btnEncrypt, BorderLayout.WEST);
		pnlButton.add(btnDecrypt, BorderLayout.EAST);
		
		// add panels to frame
		SwingApp.frame.add(pnlOpts, BorderLayout.NORTH);
		SwingApp.frame.add(pnlText, BorderLayout.CENTER);
		SwingApp.frame.add(pnlButton, BorderLayout.SOUTH);
	}
	
	// both input areas should look the same
	private static JTextArea createInputArea() {
		JTextArea txtArea = new JTextArea(15,15);
		txtArea.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		txtArea.setLineWrap(true);
		return txtArea;
	}
	
	private static void decrypt() {
		try {
			CryptoCipher cypher = getCipher();
			if (cypher != null) {
				byte[] enc = Base64Encoder.decodeBase64(txtCrypto.getText());
				txtPlain.setText(new String(cypher.decrypt(enc)));
			} 
		} catch (Exception e) {
			SwingApp.showErrorMessage("An error occured trying to decrypt your data");
			e.printStackTrace();
		}
	}
	
	private static void encrypt() {
		CryptoCipher cypher = getCipher();
		if (cypher != null) {
			byte[] enc = cypher.encrypt(txtPlain.getText().getBytes());
			txtCrypto.setText(Base64Encoder.encodeBase64(enc));
		}
	}
	
	private static void openFile() {
		if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			try {
				String txt = new String(Files.readAllBytes(Paths.get(fileChooser.getSelectedFile().getAbsolutePath())));
				txtPlain.setText(txt);
			} catch (IOException e) {
				SwingApp.showErrorMessage("Error loading file");
				e.printStackTrace();
			}
		}
	}
	
	// get cipher based on selected options
	private static CryptoCipher getCipher() {
		try {
			CryptoParameters params = new SymmetricCryptoParameters(new String(txtPassword.getPassword()));
			CryptoCipher cipher;
			if (rad3Des.isSelected()) {
				cipher = CryptoToolkit.getCipher(Cipher.TRIPLE_DES, params);
			} else if (radDes.isSelected()) {
				cipher = CryptoToolkit.getCipher(Cipher.DES, params);
			} else {
				cipher = CryptoToolkit.getCipher(Cipher.AES, params);
			}
			return cipher;
		} catch (Exception e) {
			if (e.getCause() instanceof InvalidKeyException) {
				SwingApp.showErrorMessage("You need to enable strong Java encrption");
			} else {
				SwingApp.showErrorMessage("An unknown error has occurred");
				e.printStackTrace();
			}
		}
		return null;
	}
	
	// basic app options
	private static AppInfo getAppInfo() {
		AppInfo appInfo = new AboutInfo();
		appInfo.height = 600;
		appInfo.width = 550;
		appInfo.version = CryptoConstants.APP_VERSION;
		appInfo.name = CryptoConstants.APP_NAME;
		appInfo.icon = CryptoConstants.APP_ICON;
		return appInfo;
	}
	
	// info for the about screen
	private static AboutInfo getAboutInfo() {
		AboutInfo appInfo = new AboutInfo();
		appInfo.height = 210;
		appInfo.width = 370;
		appInfo.version = CryptoConstants.APP_VERSION;
		appInfo.name = CryptoConstants.APP_NAME;
		appInfo.icon = CryptoConstants.APP_ICON;
		appInfo.contents = CryptoConstants.ABOUT_CONTENTS;
		return appInfo;
	}
	
	// create the menu bar
	private static JMenuBar createMenuBar() {
		JMenuBar menu = new JMenuBar();
		
		// File menu
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		
		JMenuItem menuFileOpen = new JMenuItem("Open");
		menuFileOpen.setMnemonic(KeyEvent.VK_O);
		menuFileOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openFile();
			}
		});
		menuFile.add(menuFileOpen);
		menuFile.addSeparator();
		
		JMenuItem menuFileExit = new JMenuItem("Exit");
		menuFileExit.setMnemonic(KeyEvent.VK_X);
		menuFileExit.addActionListener(new ExitActionListener(SwingApp.frame));
		menuFile.add(menuFileExit);
		menu.add(menuFile);
		
		// Clipboard menu
		JMenu menuClipboard = new JMenu("Clipboard");
		menuClipboard.setMnemonic(KeyEvent.VK_C);
		
		JMenuItem mnuCopyPlaintext = new JMenuItem("Copy Plaintext");
		mnuCopyPlaintext.addActionListener(new CopyToClipboardListener(txtPlain));
		JMenuItem mnuPastePlaintext = new JMenuItem("Paste Plaintext");
		mnuPastePlaintext.addActionListener(new CopyFromClipboardListener(txtPlain));
		JMenuItem mnuClearPlaintext = new JMenuItem("Clear Plaintext");
		mnuClearPlaintext.addActionListener(new ClearTextAreaListener(txtPlain));
		
		JMenuItem mnuCopyEncrypted = new JMenuItem("Copy Encrypted");
		mnuCopyEncrypted.addActionListener(new CopyToClipboardListener(txtCrypto));
		JMenuItem mnuPasteEncrypted = new JMenuItem("Paste Encrypted");
		mnuPasteEncrypted.addActionListener(new CopyFromClipboardListener(txtCrypto));
		JMenuItem mnuClearEncrypted = new JMenuItem("Clear Encrypted");
		mnuClearEncrypted.addActionListener(new ClearTextAreaListener(txtCrypto));
		
		menuClipboard.add(mnuCopyPlaintext);
		menuClipboard.add(mnuPastePlaintext);
		menuClipboard.add(mnuClearPlaintext);
		menuClipboard.addSeparator();
		menuClipboard.add(mnuCopyEncrypted);
		menuClipboard.add(mnuPasteEncrypted);
		menuClipboard.add(mnuClearEncrypted);
		menu.add(menuClipboard);

		// Help menu
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic(KeyEvent.VK_H);
		
		JMenuItem menuHelpAbout = new JMenuItem("About");
		menuHelpAbout.setMnemonic(KeyEvent.VK_A);
		menuHelpAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(SwingApp.frame, getAboutInfo());
			}
		});
		menuHelp.add(menuHelpAbout);
		menu.add(menuHelp);
		
		return menu;
	}
}
