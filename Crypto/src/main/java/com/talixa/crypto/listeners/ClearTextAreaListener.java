package com.talixa.crypto.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

public class ClearTextAreaListener implements ActionListener {
	private JTextArea target;
	
	public ClearTextAreaListener(JTextArea target) {
		this.target = target;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		target.setText("");
	}
}
