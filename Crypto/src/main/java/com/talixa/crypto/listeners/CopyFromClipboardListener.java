package com.talixa.crypto.listeners;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JTextArea;

public class CopyFromClipboardListener implements ActionListener {
	private JTextArea target;
	
	public CopyFromClipboardListener(JTextArea target) {
		this.target = target;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		try {
			target.setText((String)clpbrd.getData(DataFlavor.stringFlavor));
		} catch (UnsupportedFlavorException e1) {
			// do nothing
		} catch (IOException e1) {
			// do nothing
		}
	}
}
