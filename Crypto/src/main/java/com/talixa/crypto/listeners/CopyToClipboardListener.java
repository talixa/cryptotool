package com.talixa.crypto.listeners;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

public class CopyToClipboardListener implements ActionListener {
	private JTextArea source;
	
	public CopyToClipboardListener(JTextArea source) {
		this.source = source;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(new StringSelection(source.getText()), null);
	}
}
