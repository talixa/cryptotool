package com.talixa.crypto.shared;

public class CryptoConstants {

	public static final String APP_NAME = "Crypto Tool";
	public static final String APP_ICON = "res/crypto.png";
	public static final String APP_VERSION = "1.1";
	
	public static final String ABOUT_CONTENTS = 
			"<html><center>Crypto Tool is a simple Java-based encryption utility."+
			"<br><br>Version: " + APP_VERSION + 
			"<br>Copyright 2017, Talixa Software & Service, LLC</center></html>";
}
