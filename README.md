# Crypto Tool

This project is a simple implementation of DES, Triple DES, and AES in Java. Triple DES and AES require strong encryption be enabled for your Java implementation.

# Dependencies
This project uses [SwingLib](https://bitbucket.org/tcgerlach/swinglib) and [CryptoLib](https://bitbucket.org/tcgerlach/cryptolib). Please build and install these library before building this application.